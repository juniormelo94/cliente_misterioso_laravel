<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {


	Route::get('promotoresindex', 
		[
		    'as' => 'promotoresindex',
		    'uses' => 'ControllerTables\PromotoresController@index'
		]);
	Route::get('promotorescreate', 
		[
		    'as' => 'promotorescreate',
		    'uses' => 'ControllerTables\PromotoresController@create'
		]);
	Route::post('/store', 
		[
		    'as' => 'promotoresstore',
		    'uses' => 'ControllerTables\PromotoresController@store'
		]);
	Route::get('promotoresedit/{id}', 
		[
			'as'=> 'promotoresedit', 
			'uses' => 'ControllerTables\PromotoresController@edit'
		]);
	Route::post('promotoresupdate/{id}', 
		[
		    'as' => 'promotoresupdate',
		    'uses' => 'ControllerTables\PromotoresController@update'
		]);
	Route::get('promotoresshow/{id}', 
		[
		    'as' => 'promotoresshow',
		    'uses' => 'ControllerTables\PromotoresController@show'
		]);
	Route::post('promotoresdelete/{id}', 
		[
		    'as' => 'promotoresdelete',
		    'uses' => 'ControllerTables\PromotoresController@destroy'
		]);




	Route::group(['prefix' => 'promotores', 'middleware' => ['auth']], function () {

		Route::get('indexs', 
			[
			    'as' => 'indexs',
			    'uses' => 'ControllerTables\PromotoresController@index'
			]);
		Route::get('create', 
			[
			    'as' => 'create',
			    'uses' => 'ControllerTables\PromotoresController@create'
			]);
		Route::post('/store', 
			[
			    'as' => 'store',
			    'uses' => 'ControllerTables\PromotoresController@store'
			]);
		Route::get('show', 
			[
			    'as' => 'show',
			    'uses' => 'ControllerTables\PromotoresController@show'
			]);
		Route::get('edit/{id}', 
			[
				'as'=> 'edit', 
				'uses' => 'ControllerTables\PromotoresController@edit'
			]);
		Route::post('update/{id}', 
			[
			    'as' => 'update',
			    'uses' => 'ControllerTables\PromotoresController@update'
			]);
		Route::get('show/{id}', 
			[
			    'as' => 'show',
			    'uses' => 'ControllerTables\PromotoresController@show'
			]);
		Route::post('delete/{id}', 
			[
			    'as' => 'delete',
			    'uses' => 'ControllerTables\PromotoresController@destroy'
			]);

	});


	Route::get('/template', 
		[
		    'as' => 'template',
		    'uses' => 'ControllerTables\PromotoresController@template'
		]);









});

	Route::group(['prefix' => 'teste', 'middleware' => ['auth']], function () {

		Route::get('/index', 
			[
			    'as' => 'index',
			    'uses' => 'ControllerTables\TesteController@index'
			]);
		Route::get('/create', 
			[
			    'as' => 'create',
			    'uses' => 'ControllerTables\TesteController@create'
			]);
		Route::post('/store', 
			[
			    'as' => 'store',
			    'uses' => 'ControllerTables\TesteController@store'
			]);
		Route::get('/show', 
			[
			    'as' => 'show',
			    'uses' => 'ControllerTables\TesteController@show'
			]);
		Route::get('edit/{id}', 
			[
				'as'=> 'edit', 
				'uses' => 'ControllerTables\TesteController@edit'
			]);
		Route::post('update/{id}', 
			[
			    'as' => 'update',
			    'uses' => 'ControllerTables\TesteController@update'
			]);
		Route::get('show/{id}', 
			[
			    'as' => 'show',
			    'uses' => 'ControllerTables\TesteController@show'
			]);
		Route::post('delete/{id}', 
			[
			    'as' => 'delete',
			    'uses' => 'ControllerTables\TesteController@destroy'
			]);

	});


Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {

	Route::get('/', 'AdminController@index');
    
	Route::get('/admt', 'ControllerTables\AdministradoresController@tesste');

	Route::get('/cli', 'ControllerTables\ClientesController@teste');

	Route::get('/par', 'ControllerTables\ParceirosController@teste');


	Route::get('/template', 'ControllerTables\PromotoresController@template');
	Route::get('/sehow', 'ControllerTables\ParceirosController@show');


	// Route::get('/', 'ControllerTables\PromotoresController@teste');



	// Route::get('edit', 
	// 	[
	// 	    'as' => 'edit',
	// 	    'uses' => 'ControllerTables\PromotoresController@edit'
	// 	]);



	// Route::get('{$id}/edit', function ($id) {
	//     return 'Testando rotas get com valores' .$id;
	// });

	// Route::get('edit/$id', 'ControllerTables\PromotoresController@edit');

	// Route::get('delete/{$id}', ['as'=> 'delete', 'uses' => 'ControllerTables\PromotoresController@edit']);

	// Route::get('delete/{$id}', ['as'=> 'delete', 'uses' => 'ControllerTables\PromotoresController@edit'], function ($id) {
	//     return 'Testando rotas get com valores';
	// });

	// Route::get('edit/{$id}', ['as'=> 'edit', function ($id) {
	//     return 'Testando rotas get com valores';
	// });




	// Route::get('/index', 'ControllerTables\PromotoresController@index');
	// Route::post('/sa', 'ControllerTables\PromotoresController@store');

	Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/register', 'HomeController@index')->name('home');


});


	Route::get('/Administradores', 'ControllerTables\AdministradoresController@teste');

	Route::get('/Clientes', 'ControllerTables\ClientesController@teste');

	Route::get('/Parceiros', 'ControllerTables\ParceirosController@teste');

	Route::get('edit', function () {
	    return 'Testando rotas get com valores';
	});


Route::prefix('promotores')->group(function () {

});

// Route::get('/', 'AdminController@index');

Auth::routes();

