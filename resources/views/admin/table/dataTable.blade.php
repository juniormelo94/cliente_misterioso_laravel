@extends('admin.template.adminTemplate')

@section('tablesPromotores')

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>
    <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>NOME:</th>
                <th>CPF:</th>
                <th>E-MAIL:</th>
                <th>CIDADE:</th>
                <th>ESTADO:</th>
                <th>TELEFONE:</th>
                <th>CELULAR:</th>
                <th>VALIDO:</th>
                <th>PARCEIRO:</th>
                <th>AÇÕES:</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>NOME:</th>
                <th>CPF:</th>
                <th>E-MAIL:</th>
                <th>CIDADE:</th>
                <th>ESTADO:</th>
                <th>TELEFONE:</th>
                <th>CELULAR:</th>
                <th>VALIDO:</th>
                <th>PARCEIRO:</th>
                <th>AÇÕES:</th>
              </tr>
            </tfoot>
            <tbody>
                <tr>
                  <td>{{$dataPromotor->nome}}</td>
                  <td>{{$dataPromotor->cpf}}</td>
                  <td>{{$dataPromotor->email}}</td>
                  <td>{{$dataPromotor->cidade}}</td>
                  <td>{{$dataPromotor->estado}}</td>
                  <td>{{$dataPromotor->tel}}</td>
                  <td>{{$dataPromotor->cel}}</td>
                  <!-- <td>{{$dataPromotor->valido}}</td> -->
                  <td>{{ $dataPromotor->valido == 0 ? 'sim' : 'não' }}</td>
                  <td>{{$parceiroNome->nome}}</td>
                  <td>
                    <a href=''>
                      <i class="fas fa-trash fa-fw"></i>
                    </a>
                  </td>
                </tr>
            </tbody>
          </table>

          {!! Form::open(['route' => ['delete', $dataPromotor->id], 'method' => 'post' ]) !!}
            @method('delete')
            @csrf

            {{ method_field('post') }}

            <button class="btn btn-primary" type="submit">
                Delete
                <i class="fas fa-trash fa-fw"></i>
            </button>
          {!! Form::close() !!}

        </div>
      </div>
    </div>
  </div>

@endsection

