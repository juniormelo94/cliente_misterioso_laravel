@extends('admin.template.adminTemplate')

@section('tablesPromotores')

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>
    <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>NOME:</th>
                <th>CPF:</th>
                <th>E-MAIL:</th>
                <th>CIDADE:</th>
                <th>ESTADO:</th>
                <th>TELEFONE:</th>
                <th>CELULAR:</th>
                <th>VALIDO:</th>
                <th>PARCEIRO:</th>
                <th>AÇÕES:</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>NOME:</th>
                <th>CPF:</th>
                <th>E-MAIL:</th>
                <th>CIDADE:</th>
                <th>ESTADO:</th>
                <th>TELEFONE:</th>
                <th>CELULAR:</th>
                <th>VALIDO:</th>
                <th>PARCEIRO:</th>
                <th>AÇÕES:</th>
              </tr>
            </tfoot>
            <tbody>
              @foreach($dataPromotores as $promotor)
                <tr>
                  <td>{{$promotor->nome}}</td>
                  <td>{{$promotor->cpf}}</td>
                  <td>{{$promotor->email}}</td>
                  <td>{{$promotor->cidade}}</td>
                  <td>{{$promotor->estado}}</td>
                  <td>{{$promotor->tel}}</td>
                  <td>{{$promotor->cel}}</td>
                  <!-- <td>{{$promotor->valido}}</td> -->
                  <td>{{ $promotor->valido == 0 ? 'sim' : 'não' }}</td>
                  <td>{{$promotor->parceiros_id}}</td>
                  <td class="text-center">
                    <a class="mr-2" href=''>
                      <span class="glyphicon glyphicon-pencil icon-edite" aria-hidden="true"></span>
                      <i class="fas fa-pencil-alt fa-fw"></i>
                    </a>
                    <a href='{{ route("show", $promotor->id) }}'>
                      <!-- <span class="glyphicon glyphicon-eye-open icon-edite" aria-hidden="true"></span> -->
                      <i class="fas fa-eye fa-fw"></i>
                    </a>
                  </td>
                </tr>
              @endforeach()
            </tbody>
          </table>
        </div>
      </div>
    </div>

    {!! $dataPromotores->links() !!}

  </div>
  <!-- /.container-fluid -->

@endsection

