@extends('admin.template.adminTemplateIndex1')

@section('tablesPromotores')

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <a href='{{ route("promotorescreate") }}' class="btn btn-primary mb-3">
            Cadastrar
          </a>
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>NOME:</th>
                <th>CPF:</th>
                <th>E-MAIL:</th>
                <th>CIDADE:</th>
                <th>ESTADO:</th>
                <th>TELEFONE:</th>
                <th>CELULAR:</th>
                <th>VALIDO:</th>
                <th>PARCEIRO:</th>
                <th>AÇÕES:</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>NOME:</th>
                <th>CPF:</th>
                <th>E-MAIL:</th>
                <th>CIDADE:</th>
                <th>ESTADO:</th>
                <th>TELEFONE:</th>
                <th>CELULAR:</th>
                <th>VALIDO:</th>
                <th>PARCEIRO:</th>
                <th>AÇÕES:</th>
              </tr>
            </tfoot>
            <tbody>
                @foreach($dataPromotores as $promotor)
                <tr>
                  <td>{{$promotor->nome}}</td>
                  <td>{{$promotor->cpf}}</td>
                  <td>{{$promotor->email}}</td>
                  <td>{{$promotor->cidade}}</td>
                  <td>{{$promotor->estado}}</td>
                  <td>{{$promotor->tel}}</td>
                  <td>{{$promotor->cel}}</td>
                  <td>{{ $promotor->valido == 0 ? 'sim' : 'não' }}</td>
                  <td>{{$promotor->parceiros_id}}</td>
                  <td class="text-center">
                    <a href='{{ route("promotoresedit", $promotor->id) }}'>
                      <i class="fas fa-pencil-alt fa-fw mr-2"></i>
                    </a>
                    <a href='{{ route("promotoresshow", $promotor->id) }}'>
                      <i class="fas fa-eye fa-fw"></i>
                    </a>
                  </td>
                </tr>
                @endforeach()
            </tbody>
          </table>



        </div>
      </div>
    </div>
  </div>

@endsection

