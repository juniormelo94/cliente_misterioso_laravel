@extends('admin.template.templateTeste')

@section('content')

	<h1>Listagem da tabela</h1>



	<table>
		<tr class="tr-table">
			<th class="th-table">
				NOME:
			</th>
			<th class="th-table">
				CPF:
			</th>
			<th class="th-table">
				E-MAIL:
			</th>
			<th class="th-table">
				CIDADE:
			</th>
			<th class="th-table">
				ESTADO:
			</th>
			<th class="th-table">
				TEL:
			</th>
			<th class="th-table">
				CEL:
			</th>
			<th class="th-table">
				VALIDO:
			</th>
			<th class="th-table">
				PARCEIRO:
			</th>	
		</tr>
		<tr class="tr-table">
			<td class="td-table">
				{{$dataPromotor->nome}}
			</td>
			<td class="td-table">
				{{$dataPromotor->cpf}}
			</td>
			<td class="td-table">
				{{$dataPromotor->email}}
			</td>
			<td class="td-table">
				{{$dataPromotor->cidade}}
			</td>
			<td class="td-table">
				{{$dataPromotor->estado}}
			</td>
			<td class="td-table">
				{{$dataPromotor->tel}}
			</td>
			<td class="td-table">
				{{$dataPromotor->cel}}
			</td>
			<td class="td-table">
				{{ $dataPromotor->valido == 0 ? 'sim' : 'não' }}
				<!-- {{$dataPromotor->valido}} -->
			</td>
			<td class="td-table">
				{{$parceiroNome->nome}}
			</td>

		</tr>
	</table>

	{!! Form::open(['route' => ['promotoresdelete', $dataPromotor->id], 'method' => 'post' ]) !!}
	    @method('delete')
	    @csrf

	    {{ method_field('post') }}

		<button class="btn btn-primary" type="submit">
		    Delete
		    <span class="glyphicon glyphicon-trash " aria-hidden="true"></span>
		</button>
 		<!-- {{ Form::submit('Delete', array('class' => 'btn btn-primary')) }} -->
	{!! Form::close() !!}

@endsection


