@extends('admin.template.templateTeste')

@section('content')

	<h1>Listagem da tabela</h1>

@if( isset($errors) && count($errors) > 0 )
	<div class="alert alert-danger">
		@foreach( $errors->all() as $error )
			<p>{{$error}}</p>
		@endforeach()
	</div>
@endif()

@if( isset($dataPromotores) )
	<form method="post" action='{{ route("update", $dataPromotores->id) }}'>
@else()
	<form method="post" action="{{route('store')}}">
@endif()
		



		@csrf

<!-- 		<input type="hidden" name="_token" value="{{ csrf_token() }}"> -->

		<div class="form-row">
			<div class="form-group col-md-12">
				<input class="form-control" type="text" name="nome" @if(isset($dataPromotores)) value="{{ $dataPromotores->nome }}" @else value="{{old('nome')}}" @endif
				placeholder="Nome">
			</div>
			<div class="form-group col-md-12">
				<input class="form-control" type="text" name="cpf" @if(isset($dataPromotores)) value="{{ $dataPromotores->cpf }}" @else value="{{old('cpf')}}" @endif placeholder="CPF">
			</div>
			<div class="form-group col-md-12">
				<input class="form-control" type="email" name="email" @if(isset($dataPromotores)) value="{{ $dataPromotores->email }}" @else value="{{old('email')}}" @endif placeholder="E-mail">
			</div>
			<div class="form-group col-md-12">
				<input class="form-control" type="text" name="cidade" @if(isset($dataPromotores)) value="{{ $dataPromotores->cidade }}" @else value="{{old('cidade')}}" @endif placeholder="Cidade">
			</div>
			<div class="form-group col-md-12">
				<input class="form-control" type="text" name="estado" @if(isset($dataPromotores)) value="{{ $dataPromotores->estado }}" @else value="{{old('estado')}}" @endif placeholder="Estado">
			</div>
			<div class="form-group col-md-12">
				<input class="form-control" type="text" name="tel" @if(isset($dataPromotores)) value="{{ $dataPromotores->tel }}" @else value="{{old('tel')}}" @endif placeholder="Telefone">
			</div>
			<div class="form-group col-md-12">
				<input class="form-control" class="form-control" type="text" name="cel" @if(isset($dataPromotores)) value="{{ $dataPromotores->cel }}" @else value="{{old('cel')}}" @endif placeholder="Cidade">
			</div>

			<div class="form-group col-md-12">
			    <select class="form-control" name="valido" value="{{old('valido')}}" placeholder="Parceiro">
			      <option>Valido</option>
			      @foreach($fieldValido as $fieldVal)
			      	<option value="{{$fieldVal}}"
						@if( isset($dataPromotores) && $dataPromotores->valido == $fieldVal )
							selected
						@endif()
			      		>{{$fieldVal}}</option>
			      @endforeach()
			    </select>
			</div>
			<div class="form-group col-md-12">
			    <select class="form-control" name="parceiros_id" value="{{old('parceiros_id')}}" placeholder="Parceiro">
			      <option>parceiro</option>
			      @foreach($parceiros as $par)
			      	<option value="{{$par->id}}"
						@if( isset($dataPromotores) && $dataPromotores->parceiros_id == $par->id )
							selected
						@endif()
			      		>{{$par->nome}}</option>
			      @endforeach()
			    </select>
			</div>
		</div>

	@if( isset($dataPromotores) )
		<button class="btn btn-secondary">Editar</button>
	@else()
		<button class="btn btn-secondary">Cadastrar</button>
	@endif()

	</form>


@endsection