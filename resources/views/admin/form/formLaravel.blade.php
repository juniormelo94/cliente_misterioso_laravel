@extends('admin.template.templateTeste')

@section('content')

	<h1>Listagem da Promotores</h1>

@if( isset($errors) && count($errors) > 0 )
	<div class="alert alert-danger">
		@foreach( $errors->all() as $error )
			<p>{{$error}}</p>
		@endforeach()
	</div>
@endif()



@if( isset($dataPromotores) )
<!-- 	<form method="post" action='{{ route("update", $dataPromotores->id) }}'> -->
	{!! Form::model( $dataPromotores, ['route' => ['promotoresupdate', $dataPromotores->id], 'method' => 'post' ]) !!}
@else()
	{!! Form::open(['route' => 'promotoresstore']) !!}
@endif()

		@csrf

		<?php
        // echo '<pre>';
        // print_r($fieldValido);
        // exit();
        ?>

		<div class="form-row">
			<div class="form-group col-md-12">
				{!! Form::text('nome', null, ['placeholder' => 'Nome', 'class' => 'form-control', "required"] ) !!}
			</div>
			<div class="form-group col-md-12">
				{!! Form::text('cpf', null, ['placeholder' => 'CPF', 'class' => 'form-control', "required"] ) !!}
			</div>
			<div class="form-group col-md-12">
				{!! Form::email('email', null, ['placeholder' => 'E-mail', 'class' => 'form-control', "required"] ) !!}
			</div>
			<div class="form-group col-md-12">
				{!! Form::text('cidade', null, ['placeholder' => 'Cidade', 'class' => 'form-control', "required"] ) !!}
			</div>
			<div class="form-group col-md-12">
				{!! Form::text('estado', null, ['placeholder' => 'Estado', 'class' => 'form-control', "required"] ) !!}
			</div>
			<div class="form-group col-md-12">
				{!! Form::text('tel', null, ['placeholder' => 'Telefone', 'class' => 'form-control', "required"] ) !!}
			</div>
			<div class="form-group col-md-12">
				{!! Form::text('cel', null, ['placeholder' => 'Celular', 'class' => 'form-control', "required"] ) !!}
			</div>
			<div class="form-group col-md-12">
				{!! Form::select('valido', $fieldValido, null, ['placeholder' => 'Valido', 'class' => 'form-control', "required"]) !!}
				<!-- {!! Form::select('valido', ['sim' => 'sim', ' não' => ' não'], null, ['class' => 'form-control', "required"]) !!} -->
			</div>

			<div class="form-group col-md-12">
				{!! Form::select('parceiros_id', $parceiros, null, ['placeholder' => 'Parceiro', 'class' => 'form-control', "required"]) !!}
			</div>
		</div>

 		{{ Form::submit('Cadastrar', array('class' => 'btn btn-primary')) }}

{!! Form::close() !!}

@endsection