@extends('admin.template.templateTeste')

@section('content')

	<h1>Listagem da tabela</h1>

	<table>
		<tr class="tr-table">
			<th class="th-table">
				NOME:
			</th>
			<th class="th-table">
				CPF:
			</th>
			<th class="th-table">
				E-MAIL:
			</th>
			<th class="th-table">
				CIDADE:
			</th>
			<th class="th-table">
				ESTADO:
			</th>
			<th class="th-table">
				TEL:
			</th>
			<th class="th-table">
				CEL:
			</th>
			<th class="th-table">
				VALIDO:
			</th>
			<th class="th-table">
				PARCEIRO:
			</th>	
			<th class="th-table">
				AÇÕES:
			</th>
		</tr>
		@foreach($dataPromotores as $pro)
			<tr class="tr-table">
				<td class="td-table">
					{{$pro->nome}}
				</td>
				<td class="td-table">
					{{$pro->cpf}}
				</td>
				<td class="td-table">
					{{$pro->email}}
				</td>
				<td class="td-table">
					{{$pro->cidade}}
				</td>
				<td class="td-table">
					{{$pro->estado}}
				</td>
				<td class="td-table">
					{{$pro->tel}}
				</td>
				<td class="td-table">
					{{$pro->cel}}
				</td>
				<td class="td-table">
					{{ $pro->valido == 0 ? 'sim' : 'não' }}
<!-- 					{{$pro->valido}} -->
				</td>
				<td class="td-table">
					{{$pro->parceiros_id}}
				</td>
				<td class="td-table">
					<a href='{{ route("promotoresedit", $pro->id) }}'>
						<span class="glyphicon glyphicon-pencil icon-edite" aria-hidden="true"></span>
					</a>
					<a href='{{ route("promotoresshow", $pro->id) }}'>
						<span class="glyphicon glyphicon-eye-open icon-edite" aria-hidden="true"></span>
					</a>
				</td>
			</tr>
		@endforeach()
	</table>
	<a href="{{url('admin/promotores/create')}}" class="btn btn-secondary">
		Cadastrar
	</a>

	{!! $dataPromotores->links() !!}

@endsection

