<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampanhasParceirosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campanhas_parceiros', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('campanhas_id')->unsigned();
            $table->foreign('campanhas_id')->references('id')->on('campanhas')->onDelete('cascade');
            $table->bigInteger('parceiros_id')->unsigned();
            $table->foreign('parceiros_id')->references('id')->on('parceiros')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campanhas_parceiros');
    }
}
