<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelatoriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relatorios', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('nomeVendedor')->nullable();
            $table->string('celular')->nullable();
            $table->string('email')->nullable();
            $table->string('foipremiado')->nullable();
            $table->string('primeiraIndicacao')->nullable();
            $table->string('segundaIndicacao')->nullable();
            $table->string('segundaOutraMarca')->nullable();
            $table->text('descreva')->nullable();
            $table->string('gravouAudio')->nullable();
            $table->string('foto')->nullable();
            $table->bigInteger('promotores_id')->unsigned();
            $table->foreign('promotores_id')->references('id')->on('promotores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relatorios');
    }
}
