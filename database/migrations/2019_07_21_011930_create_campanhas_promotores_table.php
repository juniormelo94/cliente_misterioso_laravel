<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampanhasPromotoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campanhas_promotores', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('promotores_id')->unsigned();
            $table->foreign('promotores_id')->references('id')->on('promotores')->onDelete('cascade');
            $table->bigInteger('campanhas_parceiros_id')->unsigned();
            $table->foreign('campanhas_parceiros_id')->references('id')->on('campanhas_parceiros')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campanhas_promotores');
    }
}
