<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampanhasClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campanhas_clientes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('campanhas_id')->unsigned();
            $table->foreign('campanhas_id')->references('id')->on('campanhas')->onDelete('cascade');
            $table->bigInteger('clientes_id')->unsigned();
            $table->foreign('clientes_id')->references('id')->on('clientes')->onDelete('cascade');
            $table->bigInteger('lojas_id')->unsigned();
            $table->foreign('lojas_id')->references('id')->on('lojas')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campanhas_clientes');
    }
}
