<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoteiroDeVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roteiro_de_visitas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('campanhas_parceiros_id')->unsigned();
            $table->foreign('campanhas_parceiros_id')->references('id')->on('campanhas_parceiros')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roteiro_de_visitas');
    }
}
