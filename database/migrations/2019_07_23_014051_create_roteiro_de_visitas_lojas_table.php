<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoteiroDeVisitasLojasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roteiro_de_visitas_lojas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->date('datadavisita')->nullable();
            $table->string('horariodavisita')->nullable();
            $table->bigInteger('roteiro_de_visitas_id')->unsigned();
            $table->foreign('roteiro_de_visitas_id')->references('id')->on('roteiro_de_visitas')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roteiro_de_visitas_lojas');
    }
}
