<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
	public $table = 'clientes';
	/**
    * fillable fields
    *
    * @var array
    */
    public $fillable = [
		'nome',
		'cnpj',
		'responsavel',
		'email',
		'tel',
		'cel',
		'cidade',
		'estado',
        'administradores_id'
    ];
}
