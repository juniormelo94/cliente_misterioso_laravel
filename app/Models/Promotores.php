<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promotores extends Model
{

    public $table = 'promotores';
	/**
    * fillable fields
    *
    * @var array
    */
    public $fillable = [
        'nome',
        'cpf',
        'email',
        'cidade',
        'estado',
        'tel',
        'cel',
        'valido',
        'parceiros_id'
    ];

    /**
    * Validation rules
    *
    * @var array
    */
    public $rules = [
        'nome'  => 'required|min:2|max:10',
        'cpf' 	=> 'required|min:2|max:20',
        'email' => 'required|email',
        'tel'  => 'required|min:2|max:20',
    ];

}
