<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parceiros extends Model
{
    public $table = 'parceiros';
	/**
    * fillable fields
    *
    * @var array
    */
    public $fillable = [
        'nome',
        'cnpj',
        'cidade',
        'estado',
        'administradores_id',
        'clientes_id'
    ];

}
