<?php

namespace App\Http\Controllers\ControllerTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Parceiros;

class ParceirosController extends Controller
{
    
    private $parceiros;

	public function __construct(Parceiros $parceiros)
    {
    	$this->parceiros = $parceiros;
    }

    public function teste()
    {
    	$insert = $this->parceiros->insert([
    		'nome' => 'Claudio',
    		'cnpj' => '111111111111',
    		'cidade' => 'Recife',
    		'estado' => 'Pernambuco',
            'administradores_id' => 1,
            'clientes_id' => 1
    	]);

    	if($insert){
    		return 'inserido com sucesso!';
    	} else{
            return 'Não foi inserido!';
    	}
    }

    public function show()
    {

        $parcei = Parceiros::get();

        if($parcei){
            $foi = 'inserido com sucesso!';
            return view('admin.form.show', compact('parcei'));
        } else{
            $foi = 'Não foi inserido!';
            return view('junior', compact('foi'));
        }
    }



}
