<?php

namespace App\Http\Controllers\ControllerTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Administradores;

class AdministradoresController extends Controller
{

	private $administradores;

	public function __construct(Administradores $administradores)
    {
    	$this->administradores = $administradores;
    }

    public function teste()
    {
    	$insert = $this->administradores->insert([
    		'nome' => 'junior',
    		'cnpj' => 'junior',
    		'responsavel' => 'junior',
    		'cpf' => 'junior',
    		'email' => 'junior',
    		'cidade' => 'junior',
    		'estado' => 'junior',
    		'tel' => 'junior',
    		'cel' => 'junior'
    	]);

        if($insert){
            return 'inserido com sucesso!';
        } else{
            return 'Não foi inserido!';
        }
    }

}
