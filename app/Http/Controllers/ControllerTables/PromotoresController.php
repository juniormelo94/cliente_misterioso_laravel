<?php

namespace App\Http\Controllers\ControllerTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Promotores;
use App\Models\Parceiros;
 

class PromotoresController extends Controller
{

    private $promotores;
    // private $totalPage = 3;


    public function __construct(Promotores $promotores)
    {
        $this->promotores = $promotores;
    }


    public function index()
    {
        //bring all promotores and put on pages.
        // $dataPromotores = $this->promotores->paginate($this->totalPage);
        $dataPromotores = Promotores::get();

        if($dataPromotores){
            return view('admin.promotores.index1', compact('dataPromotores'));
        } else{
            return  'Não foi inserido!';
        }
    }


    public function create()
    {
        //get name and id from parceiros.
        $parceiros = Parceiros::pluck('nome', 'id');

        //array with valid form field.
        $fieldValido = ['sim', 'não'];

        return view('admin.promotores.formCreateEdit', compact('parceiros', 'fieldValido'));
    }


    public function store(Request $request)
    {
        //get form data.
        $dataFormCreate = $request->all();

        //form validation messages.
        $messages = [
            'nome.max' => 'Nome somente 3 caracteres!',
            'cpf.max' => 'Nome somente 3 caracteres!',
            'tel.min' => 'junior'
        ];

        //form validation.
        $validate = $this->validate($request, $this->promotores->rules, $messages);

        //registering data.
        $insert = $this->promotores->create($dataFormCreate);

        if($insert)
            return redirect(route('promotoresindex'));
        else
            return redirect(route('create'));
    }


    public function edit($id)
    {
        //get the data from this id.
        $dataPromotores = $this->promotores->find($id);

        //array with valid form field.
        $fieldValido = ['sim', 'não'];

        //get name and id from parceiros.
        $parceiros = Parceiros::pluck('nome', 'id');

        return view('admin.promotores.formCreateEdit', compact('parceiros', 'dataPromotores', 'fieldValido'));
    }


    public function update(Request $request, $id)
    {
        //get form data.
        $dataFormEdit = $request->all();

        //get the promotor by id.
        $dataPromotor = $this->promotores->find($id);

        //update recovered data.
        $updatePromotorData = $dataPromotor->update($dataFormEdit);

        if($updatePromotorData)
            return redirect(route('promotoresindex'));
        else
            return redirect()->route('edit', $id)->with(['errors' => 'Falha ao editar']);
    }


    public function show($id)
    {
        //get the promotor by id.
        $dataPromotor = $this->promotores->find($id);

        // get parceiro by id.
        $parceiroNome = Parceiros::find($dataPromotor->parceiros_id);

        return view('admin.promotores.show1', compact('dataPromotor', 'parceiroNome'));
    }


    public function destroy(Request $request, $id)
    {
        //get the promotor viewed by id.
        $dataShowPromotor = $this->promotores->find($id);

        //delete promotor.
        $deletePromotor = $dataShowPromotor->delete();

        if($deletePromotor)
            return redirect(route('promotoresindex'));
        else
            return "Deletar item ".$id;
    }

}
