<?php

namespace App\Http\Controllers\ControllerTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Promotores;
use App\Models\Parceiros;
 

class TesteController extends Controller
{

    private $promotores;
    private $totalPage = 3;


    public function __construct(Promotores $promotores)
    {
        $this->promotores = $promotores;
    }


    public function index()
    {
        //take all promotores.
        $dataPromotores = $this->promotores->paginate($this->totalPage);

        if($dataPromotores){
            return view('admin.table.table', compact('dataPromotores'));
        } else{
            return  'Não foi inserido!';
        }
    }


    public function create()
    {
        //get name and id from parceiros.
        // $parceiros = Parceiros::get();
        $parceiros = Parceiros::pluck('nome', 'id');

        $fieldValido = ['sim', 'não'];

         // dd($parceiros);

        return view('admin.form.formLaravel', compact('parceiros', 'fieldValido'));
        // return view('admin.form.formCreate', compact('parceiros', 'fieldValido'));
    }


    public function store(Request $request)
    {
        //get form data
        $dataFormCreate = $request->all();

        $messages = [
            'nome.max' => 'Nome somente 3 caracteres!',
            'cpf.max' => 'Nome somente 3 caracteres!',
            'tel.min' => 'junior'
        ];

        // $validate = $this->validate($request, [
        //     'nome' => 'required|min:2|max:3',
        //     'email' => 'required|email',
        //     'tel' => 'required|min:2|max:3',
        // ], $messages);

        $validate = $this->validate($request, $this->promotores->rules, $messages);
        // $this->validate($request, $rules);


        // $validator = Validator::make(Input::all());

        // dd($dataForm);

        //registering data
        $insert = $this->promotores->create($dataFormCreate);


        if($insert)
            return redirect(route('index'));
        else
            return redirect(route('create'));
    }


    public function edit($id)
    {

        //get the data from this id.
        $dataPromotores = $this->promotores->find($id);

        // $dataPromotoresWithId = $this->promotores::where('id', $id)->get();


        $fieldValido = ['sim', 'não'];

        // $parceiros = Parceiros::get();
        $parceiros = Parceiros::pluck('nome', 'id');

        return view('admin.form.formLaravel', compact('parceiros', 'dataPromotores', 'fieldValido'));
        // return view('admin.form.formCreate', compact('parceiros', 'dataPromotores', 'fieldValido'));
    }


    public function update(Request $request, $id)
    {
         $dataFormEdit = $request->all();

         $dataPromotor = $this->promotores->find($id);

         $updatePromotorData = $dataPromotor->update($dataFormEdit);

         // dd($dataPromotores);

        if($updatePromotorData)
            return redirect(route('index'));
        else
            return redirect()->route('edit', $id)->with(['errors' => 'Falha ao editar']);
    }


    public function show($id)
    {

        $dataPromotor = $this->promotores->find($id);

        $parceiroNome = Parceiros::find($dataPromotor->parceiros_id);
        // $parceiroNome = Parceiros::where('id', $dataPromotor->parceiros_id)->get('nome');
        // $parceiroNome = Parceiros::pluck('nome')->find($id);

        // dd($parceiroNome);

        // return "Show item ".$id;
        return view('admin.table.dataTable', compact('dataPromotor', 'parceiroNome'));
    }


    public function destroy(Request $request, $id)
    {
        $dataShowPromotor = $this->promotores->find($id);

        $deletePromotor = $dataShowPromotor->delete();

        if($deletePromotor)
            return redirect(route('index'));
        else
            return "Deletar item ".$id;
    }


    public function template()
    {

        //take all promotores.
        $dataPromotores = $this->promotores->paginate($this->totalPage);

        if($dataPromotores){
            return view('admin.table.table', compact('dataPromotores'));
        } else{
            return  'Não foi inserido!';
        }
    }



    public function teste()
    {
    	$id_parceiros = Parceiros::find(1);

    	$insert = Promotores::insert([
    		'nome' => 'Marcelo',
    		'cpf' => 'Marcelo',
    		'email' => 'Marcelo',
    		'cidade' => 'Marcelo',
    		'estado' => 'Marcelo',
    		'tel' => 'Marcelo',
    		'cel' => 'Marcelo',
    		'valido' => 'sim',
    		'parceiros_id' => $id_parceiros->id
    	]);

    	if($insert){
    		$foi = 'inserido com sucesso!';
    		return view('junior', compact('foi'));
    	} else{
    		$foi = 'Não foi inserido!';
    		return view('junior', compact('foi'));
    	}


        // $parceiroNome = Parceiros::find($promotor->parceiros_id);

        // //get field parceiro.
        // $nameParceiro = $request->input('parceiro');

        // //get the id parceiro.
        // $idParceiro = Parceiros::where('nome', $nameParceiro)->get('id');

        // $ola = array_push($dataForm, $idParceiro);

        // echo $idParceiro;
        // echo '<pre>';
        // print_r($dataForm);
        // exit();


        // dd($dataForm);
    }
}
