<?php

namespace App\Http\Controllers\ControllerTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Clientes;


class ClientesController extends Controller
{

    private $clientes;

	public function __construct(Clientes $clientes)
    {
    	$this->clientes = $clientes;
    }

    public function teste()
    {
    	$insert = $this->clientes->insert([
    		'nome' => 'Zo',
    		'cnpj' => 'Zé',
    		'responsavel' => 'Zé',
    		'email' => 'Zé',
    		'tel' => 'Zé',
    		'cel' => 'Zé',
    		'cidade' => 'Zé',
    		'estado' => 'Zé',
            'administradores_id' => 1
    	]);

        if($insert){
            return 'inserido com sucesso!';
        } else{
            return 'Não foi inserido!';
        }
    }

}
